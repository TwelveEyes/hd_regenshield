### Notes

---

- Regenerative shield cores will spawn very rarely in place of blue armour, and extremely rarely in place of green armour.
- Loadout code is `rsd`.
- The regenerative shield core has a maximum durability of 512 and will regenerate to this maximum.
- Regeneration will start 5 seconds after the shield has taken damage.
- It can be dropped before it fully charges without damaging its maximum protective amount.
