const HDCONST_MAXSHIELDCOOLDOWN = 175;		// 5 second cooldown before regen begins

class HDRegenShield : HDMagicShield {
	default {
		tag "regenerative shield";
	}

	int shieldCooldown;

	override void OnDrop(actor dropper) {
		HDDamageHandler.OnDrop(dropper);
		let aaa=HDMagAmmo(spawn("RegenerativeShieldCore",pos));
		aaa.vel=vel;
		aaa.target=dropper;
		if(dropper){
			aaa.amount=1;
			aaa.mags.clear();
			aaa.mags.push(amount);
			let sss=dropper.findinventory("HDRegenShield");
			if(sss){
				aaa.mags[0]+=sss.maxamount;
				sss.destroy();
			}
		}
		if(self)destroy();
	}

	override void DoEffect(){
		let hdShield = HDMagicShield(Owner.FindInventory("HDMagicShield"));
		if (hdShield) {
			// Drop into inventory instead of on the ground
			// Owner.A_GiveInventory("RegenerativeShieldCore");
			// Destroy();

			let rShield = HDMagicShield(Owner.FindInventory("HDRegenShield"));
			UseInventory(rShield);
			return;
		}
		if (shieldCooldown > HDCONST_MAXSHIELDCOOLDOWN) shieldCooldown = HDCONST_MAXSHIELDCOOLDOWN;
		if (shieldCooldown > 0) shieldCooldown--;

		// HD vanilla logic below

		if(
			owner.bcorpse
			||owner.health<1
			||owner.isfrozen()
		)return;
		if(accuracy>0)accuracy--;
		if(
			amount<1
			&&(
				maxamount<1
				||(
					owner.player
					&&!bquicktoretaliate
				)
			)
		){
			if(!bquicktoretaliate){
				let aaa=owner.spawn("SpentShield",(owner.pos.xy,owner.pos.z+owner.height*0.8));
				if(aaa){
					aaa.vel=owner.vel+(cos(owner.angle),sin(owner.angle),1.);
				}
			}
			destroy();
			return;
		}
		//replenish shields and handle breaking/unbreaking
		if(
			!bstandstill
			&&amount>maxamount
		)amount--;
		else if(
			amount<maxamount
			&&!shieldCooldown
			&&(
				(
					mass>0
				)||(
					bquicktoretaliate
					&&!(level.time&(1|2|4))
				)
			)
		){
			amount++;
			if(mass>0){
				if (amount == maxamount) FlashSparks(owner);		// Shield flash when fast charge complete
				mass--;
			}
		}
		if(
			bquicktoretaliate
			&&amount==1
			&&maxamount>1
			&&!shieldCooldown
		){
			if(hd_debug)console.printf(owner.getclassname().." shield restored!");
			FlashSparks(owner);
		}
	}

	//called from HDPlayerPawn and HDMobBase's DamageMobj
	override int,name,int,double,int,int,int HandleDamage(
		int damage,
		name mod,
		int flags,
		actor inflictor,
		actor source,
		double towound,
		int toburn,
		int tostun,
		int tobreak
	){
		actor victim=owner;
		if(
			!victim
			||(flags&(DMG_NO_FACTOR|DMG_FORCED))
			||amount<1
			||!inflictor
			||(inflictor==victim)
			||(inflictor is "HDBulletActor")
			||mod=="bleedout"
			||mod=="hot"
			||mod=="cold"
			||mod=="maxhpdrain"
			||mod=="internal"
			||mod=="holy"
			||mod=="jointlock"
			||mod=="staples"
			||mass>0		// Shield blocks nothing while fast charging
		)return damage,mod,flags,towound,toburn,tostun,tobreak;

		if(!stamina)stamina=maxamount;

		int blocked=min(amount>>1,damage,stamina>>1);
		damage-=blocked;
		bool supereffective=(
			mod=="BFGBallAttack"
			||mod=="electrical"
			||mod=="balefire"
		);

		shieldCooldown = HDCONST_MAXSHIELDCOOLDOWN;
		HDMagicShield.Deplete(victim,max(supereffective?(blocked<<2):blocked,1),self);

		if(hd_debug)console.printf("BLOCKED (not bullet)  "..blocked.."    OF  "..damage+blocked..",   "..amount.." REMAIN");

		//spawn shield debris
		vector3 sparkpos;
		if(
			inflictor
			&&inflictor!=source
		)sparkpos=inflictor.pos;
		else if(
			source
		)sparkpos=(
			victim.pos.xy+victim.radius*(source.pos.xy-victim.pos.xy).unit()
			,victim.pos.z+min(victim.height,source.height*0.6)
		);
		else sparkpos=(victim.pos.xy,victim.pos.z+victim.height*0.6);

		int shrd=max(1,blocked>>6);
		for(int i=0;i<shrd;i++){
			actor aaa=victim.spawn("ShieldSpark",sparkpos,ALLOW_REPLACE);
			aaa.vel=(frandom(-3,3),frandom(-3,3),frandom(-3,3));
		}
		
		//chance to flinch
		if(damage<1){
			if(
				!(flags&DMG_NO_PAIN)
				&&blocked>(victim.spawnhealth()>>3)
				&&random(0,255)<victim.painchance
			)hdmobbase.forcepain(victim);
		}
		return damage,mod,flags,towound,toburn,tostun,tobreak;
	}

	//called from HDBulletActor's OnHitActor
	override double,double OnBulletImpact(
		HDBulletActor bullet,
		double pen,
		double penshell,
		double hitangle,
		double deemedwidth,
		vector3 hitpos,
		vector3 vu,
		bool hitactoristall
	){
		actor victim=owner;
		if(
			!victim
			||!bullet
			||amount<1
			||mass>0		// Shield blocks nothing while fast charging
		)return pen,penshell;

		if(!stamina)stamina=maxamount;

		double bulletpower=pen*bullet.mass*0.1;
		if(bulletpower<1){
			if(frandom(0,1)<bulletpower)bulletpower=1;
			else bulletpower=0;
		}

		int depleteshield=int(min(bulletpower,amount));

		if(hd_debug)console.printf("BLOCKED  "..depleteshield.."    OF  "..int(bulletpower)..",   "..int(amount-bulletpower).." REMAIN");

		if(depleteshield<=0){
			if(!bulletpower)return 0,penshell;
			return pen,penshell;
		}

		shieldCooldown = HDCONST_MAXSHIELDCOOLDOWN;
		HDMagicShield.Deplete(victim,depleteshield,self);
		spawn("ShieldNeverBlood",bullet.pos,ALLOW_REPLACE);

		victim.vel+=(
			((victim.pos.xy,victim.pos.z+victim.height*0.5)-bullet.pos).unit()
			*depleteshield
			/victim.mass
		);
		victim.angle+=deltaangle(victim.angle,victim.angleto(bullet))*frandom(-0.005,0.03);
		victim.pitch+=frandom(-1.,1.);

		double addpenshell=min(pen,amount,stamina>>3);
		if(addpenshell>0){
			pen-=addpenshell;
			penshell+=addpenshell; //in case anything else uses this value
		}
		return pen,penshell;
	}

	states {
		use:
			TNT1 A 0 A_DropInventory(invoker.getclassname());
			fail;
	}
}

class RegenerativeShieldCore : ShieldCore {
	default {
		tag "regenerative shield core";
		hdpickup.refid "rsd";
		hdmagammo.maxperunit 512;
		inventory.pickupmessage "Picked up a regenerative shield core.";
	}

	action void A_UseRegenShield() {
		//update and cycle
		invoker.syncamount();
		int lastmag=invoker.mags.size()-1;
		if(player.cmd.buttons&BT_USE){
			invoker.mags.insert(0,invoker.mags[lastmag]);
			invoker.mags.pop();
			return;
		}

		//use the existing shield to drop it
		let sss=hdpickup(findinventory("HDMagicShield"));
		if(sss){
			useinventory(sss);
			if(sss)return;
		}
		sss=hdpickup(findinventory("HDRegenShield"));
		if(sss){
			useinventory(sss);
			if(sss)return;
		}

		A_GiveInventory("HDRegenShield");
		sss=hdpickup(findinventory("HDRegenShield"));
		if(sss){
			int togive=invoker.mags[lastmag];
			sss.bstandstill=false;
			sss.bquicktoretaliate=true;
			sss.binvbar=true;
			sss.amount=1;
			sss.maxamount=togive;
			sss.bulk=invoker.magbulk;
			sss.mass=togive-1;
			invoker.mags.pop();
			invoker.amount--;
			if(sss.amount>0)self.A_StartSound("misc/i_pkup",CHAN_BODY,CHANF_OVERLAP,0.75); //HDRegenShield.FlashSparks(self);
		}
	}

	states {
		use:
			TNT1 A 0 A_UseRegenShield();
			fail;
	}
}
