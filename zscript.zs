version "4.7"

#include "zscript/regenshield.zs"

class RegenShieldHandler : EventHandler
{
    override void CheckReplacement(ReplaceEvent e)
    {
        if(!e.Replacement)
        {
            return;
        }

        switch(e.Replacement.GetClassName())
        {
            case 'BattleArmour':
                if(random[rshieldrand]() <= 12)
                {
                    e.Replacement = "RegenerativeShieldCore";
                }
                break;
            case 'GarrisonArmour':
                if (random[rshieldrand]() <= 6)
                {
                    e.Replacement = "RegenerativeShieldCore";
                }
                break;
        }
    }
}
